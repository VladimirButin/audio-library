<?php

namespace app\components\playlist;

/**
 * Playlist generator interface
 *
 * @author olre
 */
interface PlaylistGeneratorInterface
{

  /**
   * Process commands to generate playlist
   */
  public function process();
}
