<?php

namespace app\components\cache;

use yii\base\Object;
use app\models\CollectionModel;
use app\models\TagModel;

/**
 * Cache for models
 * @author olre
 */
class Cache extends Object implements CacheInterface
{

  /**
   * Loaded collections
   * @var array
   */
  private $_collections = [];

  /**
   * Loaded tags
   * @var array
   */
  private $_tags = [];

  /**
   * Retrieve collection
   * @param string $collectionTitle
   * @return CollectionModel
   */
  public function getCollection($collectionTitle)
  {
    if (array_key_exists($collectionTitle, $this->_collections)) {
      $collection = $this->_collections[$collectionTitle];
      unset($collection->tags);
      unset($collection->files);
      unset($collection->collectionsTo);
      unset($collection->collectionsFrom);
      return $collection;
    }

    $collection = CollectionModel::find()
      ->andWhere(['title' => $collectionTitle])
      ->one();

    if (empty($collection)) {
      $collection = new CollectionModel(['title' => $collectionTitle]);
      $collection->save();
    }

    $this->_collections[$collectionTitle] = $collection;

    return $collection;
  }

  /**
   * Retrieve tag
   * @param string $tagTitle
   * @return TagModel
   */
  public function getTag($tagTitle)
  {
    if (array_key_exists($tagTitle, $this->_tags)) {
      return $this->_tags[$tagTitle];
    }

    $tag = TagModel::findOne(['title' => $tagTitle]);
    if (empty($tag)) {
      $tag = new TagModel(['title' => $tagTitle]);
      $tag->save();
    }

    $this->_tags[$tagTitle] = $tag;

    return $tag;
  }

}
