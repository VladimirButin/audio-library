<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $title
 * @property array $collections
 */
class TagModel extends ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'tags';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id'], 'integer'],
      [['title'], 'string', 'max' => Yii::$app->params['stringLength']],
      [['title'], 'required'],
      [['id', 'title'], 'unique'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'title' => 'Title',
    ];
  }

  /**
   * Collections relation
   * @return yii\db\ActiveQuery
   */
  public function getCollections()
  {
    return $this
        ->hasMany(CollectionModel::className(), ['id' => 'collection_id'])
        ->viaTable(CollectionModel::tagTableName(), ['tag_id' => 'id']);
  }

  /**
   * @inheritdoc
   */
  public function afterDelete()
  {
    parent::afterDelete();

    $this->unlinkAll('collections', true);
  }

}
