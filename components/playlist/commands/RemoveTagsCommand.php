<?php

namespace app\components\playlist\commands;

use yii\base\Object;
use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Remove tags from selected collections command
 *
 * @author olre
 */
class RemoveTagsCommand extends Object implements CommandInterface
{

    /**
     * Array of titles of tags
     * @var array
     */
    public $tags = [];

    /**
     * Execute command
     * @param PlaylistGenerator $generator
     * @param CacheInterface $cache
     */
    public function execute(PlaylistGenerator $generator, CacheInterface $cache)
    {
        $tagsIds = [];
        foreach ($this->tags as $tagTitle) {
            $tag = $cache->getTag($tagTitle);
            if ($tag) {
                $tagsIds[] = $tag->id;
            }
        }

        foreach ($generator->collections as $collection) {
            $filteredTags = [];
            foreach ($collection->tags as $tag) {
                if (!in_array($tag->id, $tagsIds)) {
                    $filteredTags[] = $tag;
                } else {
                    $collection->collection->unlink('tags', $tag, true);
                }
            }

            $collection->tags = $filteredTags;
        }
    }

}
