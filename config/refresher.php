<?php

return [
  [
    'class' => 'app\components\refresh\commands\CollectionLinkCommand',
    'collectionFromTitle' => 'artist:{artist}',
    'collectionToTitle' => 'directory:{relativePath}',
  ],
  [
    'class' => 'app\components\refresh\commands\CollectionLinkCommand',
    'collectionFromTitle' => 'directory:{relativePath}',
    'collectionToTitle' => 'track:{title}_from_{album}_by_{artist}',
  ],
  [
    'class' => 'app\components\refresh\commands\TagCommand',
    'collectionTitle' => 'artist:{artist}',
    'tagTitle' => 'artist',
  ],
  [
    'class' => 'app\components\refresh\commands\TagCommand',
    'collectionTitle' => 'directory:{relativePath}',
    'tagTitle' => 'directory',
  ],
  [
    'class' => 'app\components\refresh\commands\TagCommand',
    'collectionTitle' => 'track:{title}_from_{album}_by_{artist}',
    'tagTitle' => 'track',
  ],
  [
    'class' => 'app\components\refresh\commands\FileLinkCommand',
    'collectionTitle' => 'track:{title}_from_{album}_by_{artist}',
  ],
];
