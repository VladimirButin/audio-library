<?php

namespace app\components\playlist\commands;

use yii\base\Object;
use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Filter collections list by tags command
 *
 * @author olre
 */
class FilterCollectionsCommand extends Object implements CommandInterface
{

  /**
   * Array of titles of tags
   * @var array
   */
  public $tags = [];

  /**
   * Exclude collections with tags instead of filtering
   * @var bool
   */
  public $exclude = false;

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    $tagsIds = [];
    foreach ($this->tags as $tagTitle) {
      $tag = $cache->getTag($tagTitle);
      if ($tag) {
        $tagsIds[] = $tag->id;
      }
    }

    $filteredCollections = [];
    foreach ($generator->collections as $collection) {
      $hasTags = false;
      foreach ($collection->tags as $tag) {
        if (in_array($tag->id, $tagsIds)) {
          $hasTags = true;
          break;
        }
      }

      if (($this->exclude && !$hasTags) || (!$this->exclude && $hasTags)) {
        $filteredCollections[] = $collection;
      }
    }

    $generator->collections = $filteredCollections;
  }

}
