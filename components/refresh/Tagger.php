<?php

namespace app\components\refresh;

use yii\base\Object;
use app\components\cache\CacheInterface;
use app\models\FileModel;

/**
 * Class for managing collections and tags
 *
 * @author olre
 */
class Tagger extends Object implements TaggerInterface
{

  /**
   * Commands for tagging
   * @var array
   */
  public $commands = [];

  /**
   * Cache
   * @var CacheInterface
   */
  public $cache = null;

  /**
   * Process file
   * @param FileModel $file
   * @param type $relativePath
   * @param type $fileName
   */
  public function process(FileModel $file, $relativePath, $fileName)
  {
    $replacements = $this->generateReplaces($file, $relativePath, $fileName);

    foreach ($this->commands as $command) {
      $command->execute($this->cache, $file, $replacements);
    }
  }

  /**
   * Generate replaces array
   * @param FileModel $file
   * @param string $relativePath
   * @param string $fileName
   * @return array
   */
  protected function generateReplaces(FileModel $file, $relativePath, $fileName)
  {
    return [
      '{fullPath}' => $file->path,
      '{relativePath}' => $relativePath,
      '{fileName}' => $fileName,
      '{artist}' => $file->artist,
      '{album}' => $file->album,
      '{title}' => $file->title,
    ];
  }

}
