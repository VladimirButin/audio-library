<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Angular loading bar bundle
 */
class AngularLoadingBarAsset extends AssetBundle
{

  public $sourcePath = '@bower/angular-loading-bar/src';

  public $css = ['loading-bar.css'];
  public $js = ['loading-bar.js'];
  public $jsOptions = ['position' => View::POS_HEAD];
  public $depends = [
  ];

}
