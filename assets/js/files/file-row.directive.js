(function () {
    angular.module('alAppFiles').directive('fileRow', fileRow);

    function fileRow() {
        return {
            restrict: 'EA',
            scope: {
                file: '<',
            },
            templateUrl: '/templates/file-row.html',
        };
    }
})();