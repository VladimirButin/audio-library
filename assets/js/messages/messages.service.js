(function () {
    // Messages handling
    class MessagesService {
        constructor(messagesConstants) {
            this._messages = [];
            this._maxLength = messagesConstants.maxLength;
        }

        add(message) {
            this._messages.push(message);
            if (this._messages.length > this._maxLength) {
                this._messages.splice(0, this._messages.length - this._maxLength);
            }
        }

        remove(index) {
            this._messages.splice(index, 1);
        }

        getList() {
            return this._messages;
        }

        processErrors(errors) {
            let result = ['Errors'];

            angular.forEach(errors, function (error) {
                result.push(error.field + ': ' + error.message);
            });

            return result.join('<br>');
        }
    }

    angular.module('alAppMessages').service('MessagesService', MessagesService);
})();
