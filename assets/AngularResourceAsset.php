<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Angular resource bundle
 */
class AngularResourceAsset extends AssetBundle
{

  public $sourcePath = '@bower/angular-resource';

  public $css = [];
  public $js = ['angular-resource.js'];
  public $jsOptions = ['position' => View::POS_HEAD];
  public $depends = [
    'app\assets\AngularAsset',
  ];

}
