<?php

namespace app\components\playlist\commands;

use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Playlist generation command interface
 * @author olre
 */
interface CommandInterface
{

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache);
}
