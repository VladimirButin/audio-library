<?php

namespace app\components\refresh\commands;

use app\components\cache\CacheInterface;
use app\models\FileModel;

/**
 * Command to tag collection with tag
 *
 * @author olre
 */
class TagCommand extends AbstractCommand
{

  /**
   * Collection title
   * @var string
   */
  public $collectionTitle = "";

  /**
   * Tag title
   * @var string
   */
  public $tagTitle = "";

  /**
   * Execute command
   * @param CacheInterface $cache
   * @param FileModel $file
   * @param array $replacements
   */
  public function execute(CacheInterface $cache, FileModel $file, array $replacements)
  {
    $collection = $cache->getCollection($this->replace($this->collectionTitle, $replacements));
    $tag = $cache->getTag($this->replace($this->tagTitle, $replacements));

    $addToCollection = true;
    foreach ($collection->tags as $collectionTag) {
      if ($collectionTag->id == $tag->id) {
        $addToCollection = false;
        break;
      }
    }

    if ($addToCollection) {
      $collection->link('tags', $tag);
    }
  }

}
