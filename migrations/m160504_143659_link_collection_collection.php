<?php

use yii\db\Migration;
use app\models\CollectionModel;

class m160504_143659_link_collection_collection extends Migration
{

  public function up()
  {
    $this->createTable(CollectionModel::collectionTableName(), [
      'collection_from_id' => $this->bigInteger(),
      'collection_to_id' => $this->bigInteger(),
      'order' => $this->bigInteger(),
      ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

    $this->addPrimaryKey('c2c_pk', CollectionModel::collectionTableName(), ['collection_from_id', 'collection_to_id']);
    $this->createIndex('c2c_order_index', CollectionModel::collectionTableName(), ['collection_from_id', 'order']);
  }

  public function down()
  {
    $this->dropTable(CollectionModel::collectionTableName());
  }

}
