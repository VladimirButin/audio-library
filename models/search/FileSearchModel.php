<?php

namespace app\models\search;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\FileModel;

/**
 * This is the search model class for table "tags".
 *
 * @property integer $id
 * @property string $title
 */
class FileSearchModel extends FileModel
{

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id'], 'integer'],
      [['path'], 'string', 'max' => Yii::$app->params['stringLength']],
      [['title', 'artist', 'album'], 'string', 'max' => Yii::$app->params['stringLength']],
    ];
  }

  /**
   * Search method
   * @param array $data
   * @return ActiveDataProvider
   */
  public function search($data)
  {
    $this->setAttributes($data);
    $query = $this->find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['path' => SORT_ASC]],
      'pagination' => ['pageSize' => Yii::$app->params['pageSize']],
    ]);

    if (!$this->validate()) {
      $query->where('0=1');
      return $dataProvider;
    }

    $query
      ->andFilterWhere(['id' => $this->id])
      ->andFilterWhere(['like', 'title', $this->title])
      ->andFilterWhere(['like', 'artist', $this->artist])
      ->andFilterWhere(['like', 'album', $this->album])
      ->andFilterWhere(['like', 'path', $this->path]);

    return $dataProvider;
  }

  /**
   * Typeahead search method
   * @param array $data
   * @return array
   */
  public function typeahead($data)
  {
    $this->setAttributes($data);
    $query = $this->find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['path' => SORT_ASC]],
      'pagination' => ['pageSize' => Yii::$app->params['typeaheadLimit']],
    ]);

    if (!$this->validate()) {
      $query->where('0=1');
    }

    $query->andFilterWhere(['like', 'path', $this->path]);

    return $dataProvider->getModels();
  }

}
