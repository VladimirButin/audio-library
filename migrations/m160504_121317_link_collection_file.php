<?php

use yii\db\Migration;
use app\models\CollectionModel;

class m160504_121317_link_collection_file extends Migration
{

  public function up()
  {
    $this->createTable(CollectionModel::fileTableName(), [
      'file_id' => $this->bigInteger(),
      'collection_id' => $this->bigInteger(),
      'order' => $this->bigInteger(),
      ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

    $this->addPrimaryKey('f2c_pk', CollectionModel::fileTableName(), ['file_id', 'collection_id']);
    $this->createIndex('f2c_order_index', CollectionModel::fileTableName(), ['collection_id', 'order']);
  }

  public function down()
  {
    $this->dropTable(CollectionModel::fileTableName());
  }

}
