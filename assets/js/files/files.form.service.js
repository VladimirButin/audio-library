(function () {
    // File form service
    angular.module('alAppFiles').factory('FilesFormService', getFilesFormService);

    getFilesFormService.$inject = ['FilesCrudService', 'FilesListService', 'BaseFormService'];

    function getFilesFormService(FilesCrudService, FilesListService, BaseFormService) {
        "use strict";

        class FilesFormService extends BaseFormService {
            loadData() {
                this._CrudService
                    .loadData(this.getCurrentObject().path)
                    .then(this.loadDataCallback.bind(this));
            }

            loadDataCallback(data) {
                angular.extend(this.getCurrentObject(), data.hasOwnProperty('title')
                    ? data
                    : {
                    title: '',
                    artist: '',
                    album: ''
                });
            }
        }

        return new FilesFormService(FilesCrudService, FilesListService);
    }
})();