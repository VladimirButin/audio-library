<?php

namespace app\components\refresh;

/**
 * Refresher interface
 * @author olre
 */
interface RefresherInterface
{

  /**
   * Process directory for files
   */
  public function process();
}
