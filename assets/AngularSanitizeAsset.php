<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Angular sanitize bundle
 */
class AngularSanitizeAsset extends AssetBundle
{

  public $sourcePath = '@bower/angular-sanitize';

  public $css = [];
  public $js = ['angular-sanitize.js'];
  public $jsOptions = ['position' => View::POS_HEAD];
  public $depends = [
    'app\assets\AngularAsset',
  ];

}
