(function () {
    // Tracks form service
    angular.module('alAppTracks').factory('TracksFormService', getTracksFormService);

    getTracksFormService.$inject = ['TracksCrudService', 'TracksListService', 'BaseFormService'];

    function getTracksFormService(TracksCrudService, TracksListService, BaseFormService) {
        "use strict";

        class TagsFormService extends BaseFormService {
        }

        return new TagsFormService(TracksCrudService, TracksListService);
    }
})();