<?php

namespace app\components\playlist\commands;

use yii\base\Object;
use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Add tags to selected collections command
 *
 * @author olre
 */
class AddTagsCommand extends Object implements CommandInterface
{

    /**
     * Array of titles of tags
     * @var array
     */
    public $tags = [];

    /**
     * Execute command
     * @param PlaylistGenerator $generator
     * @param CacheInterface $cache
     */
    public function execute(PlaylistGenerator $generator, CacheInterface $cache)
    {
        foreach ($generator->collections as $collection) {
            foreach ($this->tags as $tagTitle) {
                $tag = $cache->getTag($tagTitle);

                $collection->tags[] = $tag;
                $collection->collection->link('tags', $tag);
            }
        }
    }

}
