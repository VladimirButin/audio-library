<?php

namespace app\components\cache;

use app\models\TagModel;
use app\models\CollectionModel;

/**
 * Cache interface
 * @author olre
 */
interface CacheInterface
{

  /**
   * Get tag by name
   * @param string $tagTitle
   * @return TagModel
   */
  public function getTag($tagTitle);

  /**
   * Get collection by name
   * @param string $collectionTitle
   * @return CollectionModel
   */
  public function getCollection($collectionTitle);
}
