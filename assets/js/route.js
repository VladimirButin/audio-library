(function () {
    angular.module('alApp').config(['$routeProvider',
        function ($routeProvider) {
            "use strict";
            $routeProvider
                .when('/tags', {
                    templateUrl: '/templates/tags-page.html'
                })
                .when('/files', {
                    templateUrl: '/templates/files-page.html'
                })
                .when('/collections', {
                    templateUrl: '/templates/collections-page.html'
                })
                .when('/tracks', {
                    templateUrl: '/templates/tracks-page.html'
                })
                .otherwise({
                    template: ''
                })
        }]);
})();
