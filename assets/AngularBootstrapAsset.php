<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Angular bootstrap bundle
 */
class AngularBootstrapAsset extends AssetBundle
{

  public $sourcePath = '@bower/angular-bootstrap';

  public $css = [];
  public $js = ['ui-bootstrap.js'];
  public $jsOptions = ['position' => View::POS_HEAD];
  public $depends = [
    'app\assets\AngularAsset',
  ];

}
