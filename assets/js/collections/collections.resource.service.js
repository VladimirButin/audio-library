(function () {
    // Collections resource
    angular.module('alAppCollections').factory('CollectionsResourceService', CollectionsResourceService);

    CollectionsResourceService.$inject = ['$resource'];

    function CollectionsResourceService($resource) {
        return $resource('/collections/:action/', {}, {
            create: {
                method: 'POST',
                params: {
                    action: 'create',
                }
            },
            update: {
                method: 'PUT',
                url: '/collections/:action/?id=:id',
                params: {
                    action: 'update',
                    id: '@id',
                }
            },
            view: {
                method: 'GET',
                params: {
                    action: 'view',
                },
            },
            list: {
                method: 'GET',
                params: {
                    action: 'index',
                    page: 1,
                },
            },
            typeahead: {
                method: 'GET',
                params: {
                    'action': 'typeahead',
                }
            },
            remove: {
                method: 'DELETE',
                params: {
                    action: 'delete',
                }
            },
            loadTags: {
                method: 'GET',
                params: {
                    action: 'load-tags',
                }
            },
            loadFiles: {
                method: 'GET',
                params: {
                    action: 'load-files',
                }
            },
            loadCollections: {
                method: 'GET',
                params: {
                    action: 'load-collections',
                }
            },
        }, {
            stripTrailingSlashes: false,
        });
    }
})();
