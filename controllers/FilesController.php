<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\helpers\MetaData;
use app\models\search\FileSearchModel;

class FilesController extends ActiveController
{

  public $modelClass = 'app\models\FileModel';
  public $serializer = [
    'class' => 'yii\rest\Serializer',
    'collectionEnvelope' => 'items',
  ];

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    // Application does not use any sort auth authentication
    unset($behaviors['authenticator']);
    unset($behaviors['rateLimiter']);

    return $behaviors;
  }

  public function actions()
  {
    $actions = parent::actions();
    $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

    return $actions;
  }

  public function prepareDataProvider()
  {
    return (new FileSearchModel())->search(Yii::$app->request->queryParams);
  }

  public function actionLoadData($path)
  {
    $data = MetaData::getData((string) $path);
    return $data ? $data->toArray() : false;
  }

  public function actionTypeahead()
  {
    return [
      'items' => (new FileSearchModel())->typeahead(Yii::$app->request->queryParams)
    ];
  }

}
