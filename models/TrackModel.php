<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\validators\FileExistsValidator;

/**
 * This is the model class for table "tracks".
 *
 * @property integer $id
 * @property string $path
 */
class TrackModel extends ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'tracks';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id'], 'integer'],
      [['path'], 'string', 'max' => Yii::$app->params['stringLength']],
      [['path'], FileExistsValidator::className()],
      [['id'], 'unique'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'path' => 'Path',
    ];
  }

}
