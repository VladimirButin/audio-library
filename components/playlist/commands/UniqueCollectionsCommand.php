<?php

namespace app\components\playlist\commands;

use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Unique collections command
 *
 * @author olre
 */
class UniqueCollectionsCommand implements CommandInterface
{

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    $encounteredCollections = [];
    $filteredCollections = [];

    foreach ($generator->collections as $collection) {
      if (isset($encounteredCollections[$collection->collection->id])) {
        continue;
      }

      $encounteredCollections[$collection->collection->id] = true;
      $filteredCollections[] = $collection;
    }

    $generator->collections = $filteredCollections;
  }

}
