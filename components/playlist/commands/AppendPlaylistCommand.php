<?php

namespace app\components\playlist\commands;

use app\components\playlist\PlaylistGenerator;
use app\components\cache\CacheInterface;
use app\models\TrackModel;

/**
 * Append files to playlist command
 *
 * @author olre
 */
class AppendPlaylistCommand implements CommandInterface
{

  /**
   * Append only first suitable file from collection
   * @var bool
   */
  public $onlyFirst = true;

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    foreach ($generator->collections as $collection) {
      foreach ($collection->files as $file) {
        (new TrackModel(['path' => $file->path]))->save();
        if ($this->onlyFirst) {
          break;
        }
      }
    }
  }

}
