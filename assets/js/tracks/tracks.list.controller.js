(function () {
    // Track list controller
    angular.module('alAppTracks').controller('TracksListController', TracksListController);

    TracksListController.$inject = ['PagerFactory', 'TracksCrudService', 'TracksFormService', 'TracksListService'];

    function TracksListController(PagerFactory, TracksCrudService, TracksFormService, TracksListService) {
        var vm = this;

        vm.pager = initPager();
        vm.edit = edit;
        vm.remove = remove;
        vm.filter = filter;
        vm.resetFilter = resetFilter;
        vm.tracks = [];
        vm.searchModel = TracksCrudService.init();
        vm.filterSearchModel = TracksCrudService.init();

        vm.pager.getCurrentPage();
        TracksListService.setRefresh(vm.pager.getCurrentPage.bind(vm.pager));

        function initPager() {
            return PagerFactory(handleRequest, handleData);
        }

        function handleRequest(pageNumber) {
            var searchParams = angular.copy(vm.filterSearchModel);
            searchParams.page = pageNumber;

            return TracksCrudService.list(searchParams);
        }

        function handleData(data) {
            vm.tracks = data.items;
        }

        function edit($index) {
            TracksFormService.startUpdate(vm.tracks[$index]);
        }

        function remove($index) {
            if (confirm('Are you sure want to delete track?')) {
                TracksCrudService
                    .remove(vm.tracks[$index])
                    .then(TracksListService.getRefresh());
            }
        }

        function filter() {
            angular.copy(vm.searchModel, vm.filterSearchModel);
            vm.pager.getPage(1);
        }

        function resetFilter() {
            angular.copy(vm.filterSearchModel, vm.searchModel);
        }
    }
})();