<?php

namespace app\components\refresh\commands;

use yii\base\Object;

/**
 * Abstract tagger command class
 *
 * @author olre
 */
abstract class AbstractCommand extends Object implements CommandInterface
{

  /**
   * Insert parameters to string
   * @param string $string
   * @param array $replacements
   * @return string
   */
  protected function replace($string, $replacements)
  {
    return strtr($string, $replacements);
  }

}
