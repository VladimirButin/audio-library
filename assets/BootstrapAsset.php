<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Bootstrap bundle
 */
class BootstrapAsset extends AssetBundle
{

  public $sourcePath = '@bower/bootstrap/dist';

  public $css = ['css/bootstrap.css'];
  public $js = [];
  public $jsOptions = ['position' => View::POS_HEAD];

}
