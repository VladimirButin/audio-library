(function () {
    angular.module('alAppFiles').factory('FilesCrudService', getFilesCrudService);

    getFilesCrudService.$inject = ['$q', 'FilesResourceService', 'MessagesService', 'BaseCrudService'];

    function getFilesCrudService($q, FilesResourceService, MessagesService, BaseCrudService) {
        "use strict";

        class FilesCrudService extends BaseCrudService {
            /**
             * @inheritDoc
             */
            init() {
                return {
                    id: "",
                    path: "",
                    title: "",
                    artist: "",
                    album: "",
                }
            }

            /**
             * @inheritDoc
             */
            prepareParams(object, withId) {
                var params = {
                    path: object.path,
                    title: object.title,
                    artist: object.artist,
                    album: object.album,
                };

                if (withId) {
                    params.id = object.id;
                }

                return params;
            }

            /**
             * @inheritDoc
             */
            messageText(object) {
                return 'File "' + object.path + '"';
            }

            /**
             * Load file data by path
             *
             * @param path
             * @returns {*}
             */
            loadData(path) {
                var self = this;

                return self.$q(function (resolve, reject) {
                    self.ResourceService.loadData({path: path}, function (data) {
                        self.MessagesService.add({type: 'success', text: 'File "' + path + '" has been loaded'});
                        resolve(data);
                    }, function (data) {
                        self.showErrorData(data);
                        reject(data);
                    });
                });
            }
        }

        return new FilesCrudService($q, FilesResourceService, MessagesService);
    }
})();
