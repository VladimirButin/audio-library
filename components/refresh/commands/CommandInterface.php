<?php

namespace app\components\refresh\commands;

use app\models\FileModel;
use app\components\cache\CacheInterface;

/**
 * Tagger command interface
 * @author olre
 */
interface CommandInterface
{

  /**
   * Execute tagger command
   * @param CacheInterface $cache
   * @param FileModel $file
   * @param array $replacements
   */
  public function execute(CacheInterface $cache, FileModel $file, array $replacements);
}
