(function () {
    angular.module('alAppCollections').controller('CollectionRowController', CollectionRowController);

    CollectionRowController.$inject = ['$scope', 'CollectionsCrudService', 'MessagesService'];

    function CollectionRowController($scope, CollectionsCrudService, MessagesService) {
        var vm = $scope;

        vm.expand = !!$scope.expand;

        vm.tags = [];
        vm.tagsLoaded = false;
        vm.tagsShown = false;
        vm.showTags = showTags;
        vm.hideTags = hideTags;

        vm.files = [];
        vm.filesLoaded = false;
        vm.filesShown = false;
        vm.showFiles = showFiles;
        vm.hideFiles = hideFiles;

        vm.collections = [];
        vm.collectionsLoaded = false;
        vm.collectionsShown = false;
        vm.showCollections = showCollections;
        vm.hideCollections = hideCollections;

        // Tags block

        function showTags() {
            if (!vm.tagsLoaded) {
                loadTags();
            } else {
                vm.tagsShown = true;
            }
        }

        function hideTags() {
            vm.tagsShown = false;
        }

        function loadTags() {
            CollectionsCrudService.loadTags(vm.collection).then(function (data) {
                if (data.items !== false) {
                    vm.tags = data.items;
                    vm.tagsLoaded = true;
                    vm.tagsShown = true;
                } else {
                    MessagesService.add({type: 'danger', text: 'An error occurred during retrieving tags'});
                }
            });
        }

        // Files block

        function showFiles() {
            if (!vm.filesLoaded) {
                loadFiles();
            } else {
                vm.filesShown = true;
            }
        }

        function hideFiles() {
            vm.filesShown = false;
        }

        function loadFiles() {
            CollectionsCrudService.loadFiles(vm.collection).then(function (data) {
                if (data.items !== false) {
                    vm.files = data.items;
                    vm.filesLoaded = true;
                    vm.filesShown = true;
                } else {
                    MessagesService.add({type: 'danger', text: 'An error occurred during retrieving files'});
                }
            });
        }

        // Collections block

        function showCollections() {
            if (!vm.collectionsLoaded) {
                loadCollections();
            } else {
                vm.collectionsShown = true;
            }
        }

        function hideCollections() {
            vm.collectionsShown = false;
        }

        function loadCollections() {
            CollectionsCrudService.loadCollections(vm.collection).then(function (data) {
                if (data.items !== false) {
                    vm.collections = data.items;
                    vm.collectionsLoaded = true;
                    vm.collectionsShown = true;
                } else {
                    MessagesService.add({type: 'danger', text: 'An error occurred during retrieving collections'});
                }
            });
        }


    }
})();