<?php

namespace app\components\refresh;

use yii\base\Object;
use yii\helpers\Console;
use app\helpers\MetaData;
use app\models\FileModel;

/**
 * Refresher is the class for processing files
 *
 * @author olre
 */
class Refresher extends Object implements RefresherInterface
{

  const PADDING_STEP = 2;

  /**
   * Padding for pretty output
   * @var integer
   */
  private $leftPadding = 0;

  /**
   * Do we need to update existing files
   * @var boolean
   */
  public $fullRefresh = false;

  /**
   * Starting path for processing
   * @var string
   */
  public $startPath;

  /**
   * Tagger object
   * @var TaggerInterface
   */
  public $tagger;

  /**
   * Entry point
   */
  public function process()
  {
    $this->processDir('');
  }

  /**
   * Recursively process directory
   * @param string $startPath
   * @param string $relativePath
   */
  protected function processDir($relativePath)
  {
    $this->leftPad();
    $fullPath = $this->startPath . DIRECTORY_SEPARATOR . $relativePath;
    Console::output('Processing path ' . $fullPath);

    $this->startSection();

    // Getting all files in alphanumerical order
    $files = scandir($fullPath);

    foreach ($files as $filename) {
      if ($filename === '.' || $filename === '..') {
        continue;
      }

      $nextPath = $relativePath == '' ? $filename : $relativePath . DIRECTORY_SEPARATOR . $filename;

      if (is_dir($this->startPath . DIRECTORY_SEPARATOR . $nextPath)) {
        $this->processDir($nextPath);
      } else {
        $this->processFile($relativePath, $filename);
      }
    }

    $this->endSection();

    $this->leftPad();
    Console::output('Path ' . $fullPath . ' has been processed');
    Console::output('');
  }

  /**
   * Process file
   * @param string $filename
   */
  protected function processFile($relativePath, $filename)
  {
    $this->leftPad();
    $fullPath = $this->startPath . DIRECTORY_SEPARATOR . $relativePath . DIRECTORY_SEPARATOR . $filename;
    Console::stdout('Processing file ' . $filename . ' ... ');

    $stats = MetaData::getData($fullPath);
    if ($stats === null) {
      Console::output('rejected');
      return;
    }

    $fileModel = FileModel::fetch($fullPath);
    if ($fileModel) {
      Console::stdout('already processed... ');
      if (!$this->fullRefresh) {
        Console::output('rejected');
        return;
      }
    }

    $fileModel = $fileModel ? : new FileModel;
    $fileModel->setAttributes([
      'path' => $fullPath,
      'title' => $stats->title,
      'artist' => $stats->artist,
      'album' => $stats->album,
    ]);

    if (!$fileModel->save()) {
        var_dump($fileModel->errors);
        die ('WTF');
    }


    Console::stdout('file saved... ');

    if ($this->tagger) {
      $this->tagger->process($fileModel, $relativePath, $filename);
    }

    Console::output('done');
  }

  /**
   * Aligning output
   */
  private function leftPad()
  {
    Console::stdout(str_repeat(' ', $this->leftPadding));
  }

  /**
   * Entering directory or file
   */
  private function startSection()
  {
    $this->leftPadding += self::PADDING_STEP;
  }

  /**
   * Leaving directory or file
   */
  private function endSection()
  {
    $this->leftPadding -= self::PADDING_STEP;
  }

}
