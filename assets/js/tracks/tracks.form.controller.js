(function() {
  // Track form controller
    angular.module('alAppTracks').controller('TracksFormController', TracksFormController);

  TracksFormController.$inject = ['TracksFormService'];

  function TracksFormController(TracksFormService) {
    var vm = this;

    vm.track = TracksFormService.getCurrentObject();
    vm.submit = submit;
    vm.reset = reset;
    vm.isEditing = isEditing;

    function submit() {
      "use strict";
      TracksFormService.submit();
    }

    function isEditing() {
      "use strict";
      return TracksFormService.isEditing();
    }

    function reset() {
      "use strict";
      TracksFormService.reset();
    }
  }
})();