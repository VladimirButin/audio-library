<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\CollectionModel;
use app\models\search\CollectionSearchModel;

class CollectionsController extends ActiveController
{

  public $modelClass = 'app\models\CollectionModel';
  public $serializer = [
    'class' => 'yii\rest\Serializer',
    'collectionEnvelope' => 'items',
  ];

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    // Application does not use any sort auth authentication
    unset($behaviors['authenticator']);
    unset($behaviors['rateLimiter']);

    return $behaviors;
  }

  public function actions()
  {
    $actions = parent::actions();
    $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
    unset($actions['view']);

    return $actions;
  }

  public function prepareDataProvider()
  {
    return (new CollectionSearchModel())->search(Yii::$app->request->queryParams);
  }

  public function actionTypeahead()
  {
    return [
      'items' => (new CollectionSearchModel())->typeahead(Yii::$app->request->queryParams)
    ];
  }

  public function actionLoadTags($id)
  {
    return ['items' => CollectionModel::loadTags($id)];
  }

  public function actionLoadFiles($id)
  {
    return ['items' => CollectionModel::loadFiles($id)];
  }

  public function actionLoadCollections($id)
  {
    return ['items' => CollectionModel::loadCollections($id)];
  }

  public function actionView($id)
  {
    $model = CollectionModel::findOne(['id' => $id]);
    if ($model) {
      return [
        'id' => $model->id,
        'title' => $model->title,
        'tags' => $model->loadTags($id),
        'files' => $model->loadFiles($id),
        'collections' => $model->loadCollections($id),
      ];
    }

    return false;
  }

}
