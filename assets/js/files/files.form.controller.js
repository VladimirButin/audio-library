(function () {
    // File form controller
    angular.module('alAppFiles').controller('FilesFormController', FilesFormController);

    FilesFormController.$inject = ['FilesFormService'];

    function FilesFormController(FilesFormService) {
        var vm = this;

        vm.file = FilesFormService.getCurrentObject();
        vm.submit = submit;
        vm.reset = reset;
        vm.isEditing = isEditing;
        vm.loadData = loadData;

        function submit() {
            "use strict";
            FilesFormService.submit();
        }

        function isEditing() {
            "use strict";
            return FilesFormService.isEditing();
        }

        function reset() {
            "use strict";
            FilesFormService.reset();
        }

        function loadData() {
            "use strict";
            FilesFormService.loadData();
        }
    }
})();