(function () {
    // Messages handling
    angular.module('alAppMessages').controller('MessagesController', MessagesController);

    MessagesController.$inject = ['MessagesService'];

    function MessagesController(MessagesService) {
        var vm = this;

        vm.messages = MessagesService.getList();
        vm.close = closeMessage;

        function closeMessage($index) {
            MessagesService.remove($index);
        }
    }
})();
