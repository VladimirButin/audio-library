<?php

namespace app\components\playlist\commands;

use app\components\playlist\PlaylistGenerator;
use app\components\cache\CacheInterface;
use app\models\TrackModel;

/**
 * Clear playlist command
 *
 * @author olre
 */
class ClearPlaylistCommand implements CommandInterface
{

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    TrackModel::deleteAll();
  }

}
