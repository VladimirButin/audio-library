<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Angular route bundle
 */
class AngularRouteAsset extends AssetBundle
{

    public $sourcePath = '@bower/angular-route';

    public $css = [];
    public $js = ['angular-route.js'];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $depends = [
        'app\assets\AngularAsset',
    ];

}
