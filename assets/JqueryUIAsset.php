<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Jquery UI bundle
 */
class JqueryUIAsset extends AssetBundle
{

  public $sourcePath = '@bower/jquery-ui';

  public $css = [];
  public $js = ['jquery-ui.js'];
  public $jsOptions = ['position' => View::POS_HEAD];
  public $depends = [
    'yii\web\JqueryAsset',
  ];

}
