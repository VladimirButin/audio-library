(function () {
    // Tag list controller
    angular.module('alAppTags').controller('TagsListController', TagsListController);

    TagsListController.$inject = ['PagerFactory', 'TagsCrudService', 'TagsFormService', 'TagsListService'];

    function TagsListController(PagerFactory, TagsCrudService, TagsFormService, TagsListService) {
        var vm = this;

        vm.pager = initPager();
        vm.edit = edit;
        vm.remove = remove;
        vm.filter = filter;
        vm.resetFilter = resetFilter;
        vm.tags = [];
        vm.searchModel = TagsCrudService.init();
        vm.filterSearchModel = TagsCrudService.init();

        vm.pager.getCurrentPage();
        TagsListService.setRefresh(vm.pager.getCurrentPage.bind(vm.pager));

        function initPager() {
            return PagerFactory(handleRequest, handleData);
        }

        function handleRequest(pageNumber) {
            var searchParams = angular.copy(vm.filterSearchModel);
            searchParams.page = pageNumber;

            return TagsCrudService.list(searchParams);
        }

        function handleData(data) {
            vm.tags = data.items;
        }

        function edit($index) {
            TagsFormService.startUpdate(vm.tags[$index]);
        }

        function remove($index) {
            if (confirm('Are you sure want to delete tag?')) {
                TagsCrudService
                    .remove(vm.tags[$index])
                    .then(TagsListService.getRefresh());
            }
        }

        function filter() {
            angular.copy(vm.searchModel, vm.filterSearchModel);
            vm.pager.getPage(1);
        }

        function resetFilter() {
            angular.copy(vm.filterSearchModel, vm.searchModel);
        }
    }
})();