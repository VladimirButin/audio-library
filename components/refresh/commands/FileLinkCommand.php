<?php

namespace app\components\refresh\commands;

use app\components\cache\CacheInterface;
use app\models\FileModel;

/**
 * Command to add file to collection
 *
 * @author olre
 */
class FileLinkCommand extends AbstractCommand
{

  /**
   * Collection title
   * @var string
   */
  public $collectionTitle = "";

  /**
   * Execute command
   * @param CacheInterface $cache
   * @param FileModel $file
   * @param array $replacements
   */
  public function execute(CacheInterface $cache, FileModel $file, array $replacements)
  {
    $collection = $cache->getCollection($this->replace($this->collectionTitle, $replacements));

    $addToCollection = true;
    foreach ($collection->files as $collectionFile) {
      if ($collectionFile->id == $file->id) {
        $addToCollection = false;
        break;
      }
    }

    if ($addToCollection) {
      $collection->link('files', $file, ['order' => count($collection->files) + 1]);
    }
  }

}
