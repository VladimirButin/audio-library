<?php

$refresherCommands = require(__DIR__ . '/refresher.php');
$playlistCommands = require(__DIR__ . '/playlist.php');

return [
    'adminEmail' => 'admin@example.com',
    'pageSize' => 10,
    'typeaheadLimit' => 10,
    'refresherCommands' => $refresherCommands,
    'playlistCommands' => $playlistCommands,
    'stringLength' => 333,
];
