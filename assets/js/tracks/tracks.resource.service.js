(function () {
    // Tracks resource
    angular.module('alAppTracks').factory('TracksResourceService', TracksResourceService);

    TracksResourceService.$inject = ['$resource'];

    function TracksResourceService($resource) {
        return $resource('/tracks/:action/', {}, {
            create: {
                method: 'POST',
                params: {
                    action: 'create',
                }
            },
            update: {
                method: 'PUT',
                url: '/tracks/:action/?id=:id',
                params: {
                    action: 'update',
                    id: '@id',
                }
            },
            list: {
                method: 'GET',
                params: {
                    action: 'index',
                    page: 1,
                },
            },
            remove: {
                method: 'DELETE',
                params: {
                    action: 'delete',
                }
            }
        }, {
            stripTrailingSlashes: false,
        });
    }
})();