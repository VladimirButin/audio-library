(function () {
    // Tracks list service
    angular.module('alAppTracks').service('TracksListService', getTracksListService);

    getTracksListService.$inject = ['BaseListService'];

    function getTracksListService(BaseListService) {
        "use strict";

        return new BaseListService;
    }
})();