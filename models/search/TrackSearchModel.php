<?php

namespace app\models\search;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\TrackModel;

/**
 * This is the search model class for table "tracks".
 *
 * @property integer $id
 * @property string $path
 */
class TrackSearchModel extends TrackModel
{

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id'], 'integer'],
      [['path'], 'string', 'max' => Yii::$app->params['stringLength']],
    ];
  }

  /**
   * Search method
   * @param array $data
   * @return ActiveDataProvider
   */
  public function search($data)
  {
    $this->setAttributes($data);
    $query = $this->find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_ASC]],
      'pagination' => ['pageSize' => Yii::$app->params['pageSize']],
    ]);

    if (!$this->validate()) {
      $query->where('0=1');
      return $dataProvider;
    }

    $query
      ->andFilterWhere(['id' => $this->id])
      ->andFilterWhere(['like', 'path', $this->path]);

    return $dataProvider;
  }

}
