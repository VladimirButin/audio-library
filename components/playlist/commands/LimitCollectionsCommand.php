<?php

namespace app\components\playlist\commands;

use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Limit n collections command
 *
 * @author olre
 */
class LimitCollectionsCommand implements CommandInterface
{

  /**
   * Limit value
   * @var int
   */
  public $limit = 0;

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    $generator->collections = array_slice($generator->collections, 0, $this->limit);
  }

}
