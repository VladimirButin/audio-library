(function () {
    // File list controller
    angular.module('alAppFiles').controller('FilesListController', FilesListController);

    FilesListController.$inject = ['PagerFactory', 'FilesCrudService', 'FilesFormService', 'FilesListService'];

    function FilesListController(PagerFactory, FilesCrudService, FilesFormService, FilesListService) {
        var vm = this;

        vm.pager = initPager();
        vm.edit = edit;
        vm.remove = remove;
        vm.filter = filter;
        vm.resetFilter = resetFilter;
        vm.files = [];
        vm.searchModel = FilesCrudService.init();
        vm.filterSearchModel = FilesCrudService.init();

        vm.pager.getCurrentPage();
        FilesListService.setRefresh(vm.pager.getCurrentPage.bind(vm.pager));

        function initPager() {
            return PagerFactory(handleRequest, handleData);
        }

        function handleRequest(pageNumber) {
            var searchParams = angular.copy(vm.filterSearchModel);
            searchParams.page = pageNumber;

            return FilesCrudService.list(searchParams);
        }

        function handleData(data) {
            vm.files = data.items;
        }

        function edit($index) {
            FilesFormService.startUpdate(vm.files[$index]);
        }

        function remove($index) {
            if (confirm('Are you sure want to delete file?')) {
                FilesCrudService
                    .remove(vm.files[$index])
                    .then(FilesListService.getRefresh());
            }
        }

        function filter() {
            angular.copy(vm.searchModel, vm.filterSearchModel);
            vm.pager.getPage(1);
        }

        function resetFilter() {
            angular.copy(vm.filterSearchModel, vm.searchModel);
        }
    }
})();