(function () {
    angular.module('alAppTags').factory('TagsCrudService', getTagsCrudService);

    getTagsCrudService.$inject = ['$q', 'TagsResourceService', 'MessagesService', 'BaseCrudService'];

    function getTagsCrudService($q, TagsResourceService, MessagesService, BaseCrudService) {
        "use strict";

        class TagsCrudService extends BaseCrudService {
            /**
             * @inheritDoc
             */
            init() {
                return {
                    id: "",
                    title: "",
                }
            }

            /**
             * @inheritDoc
             */
            prepareParams(object, withId) {
                var params = {
                    title: object.title
                };

                if (withId) {
                    params.id = object.id;
                }

                return params;
            }

            /**
             * @inheritDoc
             */
            messageText(object) {
                return 'Tag "' + object.title + '"';
            }
        }

        return new TagsCrudService($q, TagsResourceService, MessagesService);
    }
})();
