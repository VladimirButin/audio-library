<?php

namespace app\models\search;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\TagModel;

/**
 * This is the search model class for table "tags".
 *
 * @property integer $id
 * @property string $title
 */
class TagSearchModel extends TagModel
{

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id'], 'integer'],
      [['title'], 'string', 'max' => Yii::$app->params['stringLength']],
    ];
  }

  /**
   * Search method
   * @param array $data
   * @return ActiveDataProvider
   */
  public function search($data)
  {
    $this->setAttributes($data);
    $query = $this->find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['title' => SORT_ASC]],
      'pagination' => ['pageSize' => Yii::$app->params['pageSize']],
    ]);

    if (!$this->validate()) {
      $query->where('0=1');
      return $dataProvider;
    }

    $query
      ->andFilterWhere(['id' => $this->id])
      ->andFilterWhere(['like', 'title', $this->title]);

    return $dataProvider;
  }

  /**
   * Typeahead search method
   * @param array $data
   * @return array
   */
  public function typeahead($data)
  {
    $this->setAttributes($data);
    $query = $this->find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['title' => SORT_ASC]],
      'pagination' => ['pageSize' => Yii::$app->params['typeaheadLimit']],
    ]);

    if (!$this->validate()) {
      $query->where('0=1');
    }

    $query->andFilterWhere(['like', 'title', $this->title]);

    return $dataProvider->getModels();
  }

}
