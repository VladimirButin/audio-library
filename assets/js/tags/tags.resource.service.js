(function () {
    // Tags resource
    angular.module('alAppTags').factory('TagsResourceService', TagsResourceService);

    TagsResourceService.$inject = ['$resource'];

    function TagsResourceService($resource) {
        return $resource('/tags/:action/', {}, {
            create: {
                method: 'POST',
                params: {
                    action: 'create',
                }
            },
            update: {
                method: 'PUT',
                url: '/tags/:action/?id=:id',
                params: {
                    action: 'update',
                    id: '@id',
                }
            },
            list: {
                method: 'GET',
                params: {
                    action: 'index',
                    page: 1,
                },
            },
            typeahead: {
                method: 'GET',
                params: {
                    'action': 'typeahead',
                }
            },
            remove: {
                method: 'DELETE',
                params: {
                    action: 'delete',
                }
            }
        }, {
            stripTrailingSlashes: false,
        });
    }
})();