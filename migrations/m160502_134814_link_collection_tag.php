<?php

use yii\db\Migration;
use app\models\CollectionModel;
use app\models\TagModel;

class m160502_134814_link_collection_tag extends Migration
{

  public function up()
  {
    $this->createTable(CollectionModel::tagTableName(), [
      'tag_id' => $this->bigInteger(),
      'collection_id' => $this->bigInteger(),
    ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

    $this->addPrimaryKey('t2c_pk', CollectionModel::tagTableName(), ['tag_id', 'collection_id']);
  }

  public function down()
  {
    $this->dropTable(CollectionModel::tagTableName());
  }

}
