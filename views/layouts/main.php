<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use app\assets\ScriptAsset;

AppAsset::register($this);
ScriptAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="alApp">
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
      <title>Audio Library</title>
    <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
          'brandLabel' => 'Audio Library',
          'brandUrl' => Yii::$app->homeUrl,
          'options' => [
            'class' => 'navbar-inverse',
          ],
        ]);
        echo Nav::widget([
            'activateItems' => false,
          'options' => ['class' => 'navbar-nav navbar-right'],
          'items' => [
              ['label' => 'Tags', 'url' => ['site/index', '#' => '/tags']],
              ['label' => 'Files', 'url' => ['site/index', '#' => '/files']],
              ['label' => 'Collections', 'url' => ['site/index', '#' => 'collections']],
              ['label' => 'Playlist', 'url' => ['site/index', '#' => 'tracks']],
          ],
        ]);
        NavBar::end();
        ?>

      <div class="container">
        <div ng-controller="MessagesController as vm">
          <uib-alert 
              ng-repeat="message in vm.messages"
              template-url="/uib/template/alert/alert.html"
              type="{{message.type}}" 
              close="vm.close($index)">
            <span ng-bind-html="message.text"></span>
          </uib-alert>
        </div>
          <ng-view><?= $content ?></ng-view>
      </div>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="pull-right"><?= Yii::powered() ?></p>
      </div>
    </footer>

    <?php $this->endBody() ?>
  </body>
</html>

<?php $this->endPage() ?>
