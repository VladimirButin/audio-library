(function () {
    angular.module('alAppConstants', []);
    angular.module('alAppMessages', ['alAppConstants']);
    angular.module('alAppPager', []);

    angular.module('alAppBase', ['alAppMessages', 'alAppPager', 'ngResource', 'ngSanitize', 'ui.bootstrap']);
    angular.module('alAppTags', ['alAppBase']);
    angular.module('alAppFiles', ['alAppBase']);
    angular.module('alAppCollections', ['alAppBase', 'ui.sortable']);
    angular.module('alAppTracks', ['alAppBase']);

    angular.module('alApp', ['ngRoute', 'angular-loading-bar', 'alAppTags', 'alAppFiles', 'alAppCollections', 'alAppTracks']);
})();