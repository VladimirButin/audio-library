(function () {
    // Collection list controller
    angular.module('alAppCollections').controller('CollectionsListController', CollectionsListController);

    CollectionsListController.$inject = ['PagerFactory', 'CollectionsCrudService', 'CollectionsFormService', 'CollectionsListService'];

    function CollectionsListController(PagerFactory, CollectionsCrudService, CollectionsFormService, CollectionsListService) {
        var vm = this;

        vm.pager = initPager();
        vm.edit = edit;
        vm.remove = remove;
        vm.filter = filter;
        vm.resetFilter = resetFilter;
        vm.collections = [];
        vm.searchModel = CollectionsCrudService.init();
        vm.filterSearchModel = CollectionsCrudService.init();

        vm.pager.getCurrentPage();
        CollectionsListService.setRefresh(vm.pager.getCurrentPage.bind(vm.pager));

        function initPager() {
            return PagerFactory(handleRequest, handleData);
        }

        function handleRequest(pageNumber) {
            var searchParams = angular.copy(vm.filterSearchModel);
            searchParams.page = pageNumber;

            return CollectionsCrudService.list(searchParams);
        }

        function handleData(data) {
            vm.collections = data.items;
        }

        function edit($index) {
            CollectionsFormService.startUpdate(vm.collections[$index]);
        }

        function remove($index) {
            if (confirm('Are you sure want to delete collection?')) {
                CollectionsCrudService
                    .remove(vm.collections[$index])
                    .then(CollectionsListService.getRefresh());
            }
        }

        function filter() {
            angular.copy(vm.searchModel, vm.filterSearchModel);
            vm.pager.getPage(1);
        }

        function resetFilter() {
            angular.copy(vm.filterSearchModel, vm.searchModel);
        }
    }
})();