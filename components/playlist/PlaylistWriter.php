<?php

namespace app\components\playlist;

use yii\base\Object;
use app\models\TrackModel;

/**
 * Class to generate m3u playlist
 *
 * @author olre
 */
class PlaylistWriter extends Object
{

  public function write()
  {
    ob_start();
    ob_implicit_flush(false);

    echo "#EXTM3U" . PHP_EOL;

    $query = TrackModel::find();
    foreach ($query->each() as $track) {
      echo $track->path . PHP_EOL;
    }

    return ob_get_clean();
  }

}
