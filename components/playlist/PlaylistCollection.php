<?php

namespace app\components\playlist;

use yii\base\Object;
use yii\base\InvalidParamException;
use app\models\CollectionModel;

/**
 * Playlist collection
 * This class is used instead of collection entity while working with playlist is happening
 * Useful due to ability to expand related collections and work with cumulative tags and files
 *
 * @author olre
 */
class PlaylistCollection extends Object
{

  /**
   * Collection object
   * @var CollectionModel
   */
  public $collection;

  /**
   *
   * @var array
   */
  public $files = [];

  /**
   *
   * @var array
   */
  public $tags = [];

  /**
   * @inheritdoc
   * @throws InvalidParamException
   */
  public function init()
  {
    parent::init();

    if (empty($this->collection) || !($this->collection instanceof CollectionModel)) {
      throw new InvalidParamException('collection must be instance of CollectionModel');
    }

    $this->tags = $this->collection->tags;
    $this->files = $this->collection->files;
  }

  /**
   * Get all child collections
   * @return array
   */
  public function expand()
  {
    $result = [];

    foreach ($this->collection->collectionsTo as $collection) {
      $collectionWrap = new self(['collection' => $collection]);
      $collectionWrap->tags = array_merge($collectionWrap->tags, $this->tags);
        $collectionWrap->files = array_merge($collectionWrap->files, $this->files);
      $result[] = $collectionWrap;
    }

    return $result;
  }

}
