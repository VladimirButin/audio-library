(function () {
    class BaseCrudService {
        /**
         * Base crud class
         *
         * @param $q
         * @param ResourceService
         * @param MessagesService
         */
        constructor($q, ResourceService, MessagesService) {
            this.$q = $q;
            this.ResourceService = ResourceService;
            this.MessagesService = MessagesService;
        }

        /**
         * Factory method
         * Returns empty entity
         */
        init() {
            throw new Error('Initialization is not implemented');
        }

        /**
         * Factory method
         * Return entity constructed by params
         *
         * @param object
         * @param withId
         */
        prepareParams(object, withId) {
            throw new Error('Params preparation is not implemented');
        }

        /**
         * Returns title of object for messaging purposes
         *
         * @param object
         */
        messageText(object) {
            throw new Error('Message text getter is not implemented');
        }

        /**
         * Echoes error data to message service
         *
         * @param data
         */
        showErrorData(data) {
            this.MessagesService.add({type: 'danger', text: this.MessagesService.processErrors(data.data)});
        }

        /**
         * Creates object by using resource service
         * Returns promise
         *
         * @param object
         * @returns {*}
         */
        create(object) {
            var self = this;

            return self.$q(function (resolve, reject) {
                var params = self.prepareParams(object);

                self.ResourceService.create(params, function (data) {
                    self.MessagesService.add({type: 'success', text: self.messageText(object) + ' has been added'});
                    resolve(data);
                }, function (data) {
                    self.showErrorData(data);
                    reject(data);
                });
            });
        }

        /**
         * Updates object by using resource service
         * Returns promise
         *
         * @param object
         * @returns {*}
         */
        update(object) {
            var self = this;

            return self.$q(function (resolve, reject) {
                var params = self.prepareParams(object, true);

                self.ResourceService.update(params, function (data) {
                    self.MessagesService.add({type: 'success', text: self.messageText(object) + ' has been updated'});
                    resolve(data);
                }, function (data) {
                    self.showErrorData(data);
                    reject(data);
                });
            });
        }

        /**
         * Deletes object by id by using resource service
         * Returns promise
         *
         * @param object
         * @returns {*}
         */
        remove(object) {
            var self = this;

            return self.$q(function (resolve, reject) {
                var params = {
                    id: object.id
                };

                self.ResourceService.remove(params, function (data) {
                    self.MessagesService.add({
                        type: 'success', text: self.messageText(object) + ' has been has been deleted'
                    });
                    resolve(data);
                }, function (data) {
                    self.showErrorData(data);
                    reject(data);
                });
            });
        }

        /**
         * Retrieves list of objects by using resource service
         * Returns promise
         *
         * @param searchParams
         * @returns {*}
         */
        list(searchParams) {
            var self = this;

            return self.$q(function (resolve, reject) {
                self.ResourceService.list(searchParams, function (data) {
                    resolve(data);
                }, function (data) {
                    self.showErrorData(data);
                    reject(data);
                });
            });
        }

        /**
         * Retrieves list of objects by using resource service for typeahead
         * Returns promise
         *
         * @param searchParams
         * @returns {*}
         */
        typeahead(searchParams) {
            var self = this;

            return self.$q(function (resolve, reject) {
                self.ResourceService.typeahead(searchParams, function (data) {
                    resolve(data);
                }, function (data) {
                    self.showErrorData(data);
                    reject(data);
                });
            });
        }

    }

    angular.module('alAppBase').value('BaseCrudService', BaseCrudService);
})();
