<?php

namespace app\components\playlist\commands;

use yii\base\Object;
use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistCollection;
use app\components\playlist\PlaylistGenerator;
use app\models\CollectionModel;

/**
 * Add collections to list command
 *
 * @author olre
 */
class AddCollectionsCommand extends Object implements CommandInterface
{

  /**
   * Id
   * @var int
   */
  public $id;

  /**
   * Title
   * @var string
   */
  public $title;

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    $query = CollectionModel::find();
    $query
      ->with(['tags', 'files', 'collectionsTo', 'collectionsFrom'])
      ->andFilterWhere(['id' => $this->id])
      ->andFilterWhere(['like', 'title', $this->title, false]);

    foreach ($query->each() as $collection) {
      $generator->collections[] = new PlaylistCollection(['collection' => $collection]);
    }
  }

}
