<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Application bundle
 */
class AppAsset extends AssetBundle
{

  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = ['css/site.css'];
  public $js = [];
  public $depends = [
    'app\assets\BootstrapAsset',
    'app\assets\AngularAsset',
    'app\assets\AngularResourceAsset',
    'app\assets\AngularSanitizeAsset',
    'app\assets\AngularBootstrapAsset',
    'app\assets\AngularLoadingBarAsset',
    'app\assets\AngularSortableAsset',
      'app\assets\AngularRouteAsset',
  ];

}
