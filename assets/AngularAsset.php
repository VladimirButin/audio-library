<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Angular bundle
 */
class AngularAsset extends AssetBundle
{

  public $sourcePath = '@bower/angular';

  public $css = [];
  public $js = ['angular.js'];
  public $jsOptions = ['position' => View::POS_HEAD];
  public $depends = [
    'yii\web\JqueryAsset',
  ];

}
