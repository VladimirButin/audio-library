<?php

use yii\db\Migration;
use app\models\FileModel;

class m160423_163047_table_files extends Migration
{

  public function up()
  {
    $this->createTable(FileModel::tableName(), [
      'id' => $this->primaryKey(),
      'path' => $this->string(Yii::$app->params['stringLength'])->notNull()->unique(),
      'hash' => $this->string(Yii::$app->params['stringLength'])->notNull(),
      'title' => $this->string(Yii::$app->params['stringLength'])->notNull(),
      'artist' => $this->string(Yii::$app->params['stringLength'])->notNull(),
      'album' => $this->string(Yii::$app->params['stringLength'])->notNull(),
      ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

    $this->createIndex('hash_files_i', 'files', 'hash');
  }

  public function down()
  {
    $this->dropTable('files');
  }

}
