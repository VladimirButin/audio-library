<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Script asset bundle
 */
class ScriptAsset extends AssetBundle
{

    public $sourcePath = '@app/assets/js';
    public $css = [];
    public $js = [
        'app/al.app.js',
        'constants.js',
        'route.js',
        'pager/pager.factory.js',
        'messages/messages.service.js',
        'messages/messages.controller.js',

        'base/base.crud.service.js',
        'base/base.list.service.js',
        'base/base.form.service.js',

        'tags/tag-row.directive.js',
        'tags/tags.resource.service.js',
        'tags/tags.crud.service.js',
        'tags/tags.form.service.js',
        'tags/tags.list.service.js',
        'tags/tags.form.controller.js',
        'tags/tags.list.controller.js',

        'files/file-row.directive.js',
        'files/files.resource.service.js',
        'files/files.crud.service.js',
        'files/files.form.service.js',
        'files/files.list.service.js',
        'files/files.form.controller.js',
        'files/files.list.controller.js',

        'collections/collection.row.controller.js',
        'collections/collection-row.directive.js',
        'collections/collections.resource.service.js',
        'collections/collections.crud.service.js',
        'collections/collections.form.service.js',
        'collections/collections.list.service.js',
        'collections/collections.form.controller.js',
        'collections/collections.list.controller.js',

        'tracks/track-row.directive.js',
        'tracks/tracks.resource.service.js',
        'tracks/tracks.crud.service.js',
        'tracks/tracks.form.service.js',
        'tracks/tracks.list.service.js',
        'tracks/tracks.form.controller.js',
        'tracks/tracks.list.controller.js',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $depends = [
        'app\assets\AppAsset'
    ];

}
