<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\playlist\PlaylistWriter;

class SiteController extends Controller
{

  public function actions()
  {
    return array_merge(parent::actions(), [
      'error' => \yii\web\ErrorAction::className()
    ]);
  }

  public function actionIndex()
  {
    return $this->render('index');
  }

  public function actionPlaylist()
  {
    $content = (new PlaylistWriter)->write();
    Yii::$app->getResponse()->sendContentAsFile($content, 'playlist.m3u', ['mimeType' => 'audio/x-mpegurl']);
  }

}
