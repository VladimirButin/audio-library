<?php

use yii\db\Migration;
use app\models\CollectionModel;

class m160501_140923_table_collections extends Migration
{

  public function up()
  {
    $this->createTable(CollectionModel::tableName(), [
      'id' => $this->primaryKey(),
      'title' => $this->string(Yii::$app->params['stringLength'])->notNull()->unique(),
      ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
  }

  public function down()
  {
    $this->dropTable('collections');
  }

}
