<?php

use yii\db\Migration;
use app\models\TagModel;

class m160221_144429_table_tags extends Migration
{

  public function safeUp()
  {
    $this->createTable(TagModel::tableName(), [
      'id' => $this->primaryKey(),
      'title' => $this->string(Yii::$app->params['stringLength'])->notNull()->unique(),
      ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
  }

  public function safeDown()
  {
    $this->dropTable('tags');
  }

}
