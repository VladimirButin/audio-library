Audio Library
============================

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      components/         contains worker classes
      config/             contains application configurations
      controllers/        contains Web controller classes
      docker/             contains Docker data
      helpers/            contains helpers
      migrations/         contains migrations
      models/             contains model classes
      runtime/            contains files generated during runtime
      validators/         contains custom validators
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0.


INSTALLATION
------------

For development process application uses Docker. Configuration files are in docker
directory. If you want to use native environment, application requires PHP >= 5.6
and Mysql >= 5.0. Make sure, that you created database and installed application via composer.


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=al',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
```

### Refresher

For automatically filling tables with existing data, edit the file `config/refresher.php`
Example:
```php

return [
  [
    'class' => 'app\components\refresh\commands\CollectionLinkCommand',
    'collectionFromTitle' => 'artist:{artist}',
    'collectionToTitle' => 'album:{album}_by_{artist}',
  ],
  [
    'class' => 'app\components\refresh\commands\CollectionLinkCommand',
    'collectionFromTitle' => 'album:{album}_by_{artist}',
    'collectionToTitle' => 'track:{title}_from_{album}_by_{artist}',
  ],
  [
    'class' => 'app\components\refresh\commands\TagCommand',
    'collectionTitle' => 'artist:{artist}',
    'tagTitle' => 'artist',
  ],
  [
    'class' => 'app\components\refresh\commands\TagCommand',
    'collectionTitle' => 'album:{album}_by_{artist}',
    'tagTitle' => 'album',
  ],
  [
    'class' => 'app\components\refresh\commands\TagCommand',
    'collectionTitle' => 'track:{title}_from_{album}_by_{artist}',
    'tagTitle' => 'track',
  ],
  [
    'class' => 'app\components\refresh\commands\FileLinkCommand',
    'collectionTitle' => 'track:{title}_from_{album}_by_{artist}',
  ],
];
```
Will create tags artist, album and track, collection for artist, album and tracks,
link album to artist and track to album and lastly link file to track collection.

### Playlist

Playlist can be filled automatically just as refresher. Example:
```php

return [
  [
    'class' => 'app\components\playlist\commands\ClearPlaylistCommand',
  ],
  [
    'class' => 'app\components\playlist\commands\AddCollectionsCommand',
    'title' => 'artist:Front Line Assembly',
  ],
  [
    'class' => 'app\components\playlist\commands\ExpandCollectionsCommand',
    'tags' => ['album'],
  ],
  [
    'class' => 'app\components\playlist\commands\ShuffleCollectionsCommand',
  ],
  [
    'class' => 'app\components\playlist\commands\LimitCollectionsCommand',
    'limit' => 3,
  ],
  [
    'class' => 'app\components\playlist\commands\ExpandCollectionsCommand',
    'tags' => ['track'],
  ],
  [
    'class' => 'app\components\playlist\commands\UniqueCollectionsCommand',
  ],
  [
    'class' => 'app\components\playlist\commands\AppendPlaylistCommand',
  ],
];
```

Will clear current playlist, add artist FLA to collections list, take all it's albums,
shuffle them, take first 3 of them, take all it's tracks, left only unique files
and append files to playlist.