(function () {
    angular.module('alAppCollections').factory('CollectionsCrudService', getCollectionsCrudService);

    getCollectionsCrudService.$inject = ['$q', 'CollectionsResourceService', 'MessagesService', 'BaseCrudService'];

    function getCollectionsCrudService($q, CollectionsResourceService, MessagesService, BaseCrudService) {
        class CollectionsCrudService extends BaseCrudService {
            /**
             * @inheritDoc
             */
            init() {
                return {
                    id: "",
                    title: "",
                    tags: [],
                    files: [],
                    collections: [],
                }
            }

            /**
             * @inheritDoc
             */
            prepareParams(object, withId) {
                var params = {
                    title: object.title,
                    tags_id: object.tags.map(function (tag) {
                        return tag.id;
                    }),
                    files_id: object.files.map(function (file) {
                        return file.id;
                    }),
                    collections_id: object.collections.map(function (collection) {
                        return collection.id;
                    }),
                };
                if (withId) {
                    params.id = object.id;
                }

                return params;

            }

            /**
             * @inheritDoc
             */
            messageText(object) {
                return 'Collection "' + object.title + '"';
            }

            /**
             * Load all related tags by collection id by using resource
             * Returns promise
             *
             * @param collection
             * @returns {*}
             */
            loadTags(collection) {
                var self = this;

                return self.$q(function (resolve, reject) {
                    var params = {
                        id: collection.id
                    };

                    self.ResourceService.loadTags(params, function (data) {
                        resolve(data);
                    }, function (data) {
                        self.showErrorData(data);
                        reject(data);
                    });
                });
            }

            /**
             * Load all related files by collection id by using resource
             * Returns promise
             *
             * @param collection
             * @returns {*}
             */
            loadFiles(collection) {
                var self = this;

                return self.$q(function (resolve, reject) {
                    var params = {
                        id: collection.id
                    };

                    self.ResourceService.loadFiles(params, function (data) {
                        resolve(data);
                    }, function (data) {
                        self.showErrorData(data);
                        reject(data);
                    });
                });
            }

            /**
             * Load all related collections by collection id by using resource
             * Returns promise
             *
             * @param collection
             * @returns {*}
             */
            loadCollections(collection) {
                var self = this;

                return self.$q(function (resolve, reject) {
                    var params = {
                        id: collection.id
                    };

                    self.ResourceService.loadCollections(params, function (data) {
                        resolve(data);
                    }, function (data) {
                        self.showErrorData(data);
                        reject(data);
                    });
                });
            }

            /**
             * Get collection data by id by using resource
             * Returns promise
             *
             * @param id
             * @returns {*}
             */
            view(id) {
                var self = this;

                return self.$q(function (resolve, reject) {
                    var params = {
                        id: id
                    };

                    self.ResourceService.view(params, function (data) {
                        resolve(data);
                    }, function (data) {
                        self.showErrorData(data);
                        reject(data);
                    });
                });
            }
        }

        return new CollectionsCrudService($q, CollectionsResourceService, MessagesService);
    }
})();
