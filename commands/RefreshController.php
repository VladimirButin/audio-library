<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;
use app\components\cache\Cache;
use app\components\refresh\Refresher;
use app\components\refresh\Tagger;

class RefreshController extends Controller
{

  public $fullRefresh = false;

  public function options($actionID)
  {
    return ['fullRefresh'];
  }

  /**
   * Entry point
   * @param type $path
   */
  public function actionIndex($path)
  {
    ini_set('memory_limit', '-1');

    $tagger = new Tagger([
      'cache' => new Cache(),
      'commands' => array_map(function ($commandArray) {
          return Yii::createObject($commandArray);
        }, Yii::$app->params['refresherCommands']),
    ]);

    $path = FileHelper::normalizePath($path);

    $refresher = new Refresher([
      'fullRefresh' => $this->fullRefresh,
      'startPath' => $path,
      'tagger' => $tagger,
    ]);
    $refresher->process();
  }

}
