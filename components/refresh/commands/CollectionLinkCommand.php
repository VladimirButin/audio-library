<?php

namespace app\components\refresh\commands;

use app\components\cache\CacheInterface;
use app\models\FileModel;

/**
 * Command to link to collection
 *
 * @author olre
 */
class CollectionLinkCommand extends AbstractCommand
{

  /**
   * Collection from title
   * @var string
   */
  public $collectionFromTitle = "";

  /**
   * Collection to title
   * @var string
   */
  public $collectionToTitle = "";

  /**
   * Execute command
   * @param CacheInterface $cache
   * @param FileModel $file
   * @param array $replacements
   */
  public function execute(CacheInterface $cache, FileModel $file, array $replacements)
  {
    $collectionFrom = $cache->getCollection($this->replace($this->collectionFromTitle, $replacements));
    $collectionTo = $cache->getCollection($this->replace($this->collectionToTitle, $replacements));

    $addToCollection = true;
    foreach ($collectionFrom->collectionsTo as $collection) {
      if ($collection->id == $collectionTo->id) {
        $addToCollection = false;
        break;
      }
    }

    if ($addToCollection) {
      $collectionFrom->link('collectionsTo', $collectionTo, ['order' => count($collectionFrom->collectionsTo) + 1]);
    }
  }

}
