(function () {
    // Tags list service
    angular.module('alAppTags').factory('TagsListService', getTagsListService);

    getTagsListService.$inject = ['BaseListService'];

    function getTagsListService(BaseListService) {
        "use strict";

        return new BaseListService;
    }
})();