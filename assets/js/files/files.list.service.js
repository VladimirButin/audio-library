(function () {
    // Files list service
    angular.module('alAppFiles').service('FilesListService', getFilesListService);

    getFilesListService.$inject = ['BaseListService'];

    function getFilesListService(BaseListService) {
        "use strict";

        return new BaseListService;
    }
})();