<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "collections".
 *
 * @property integer $id
 * @property string $title
 * @property array $tags
 * @property array $files
 * @property array $collectionsFrom
 * @property array $collectionsTo
 */
class CollectionModel extends ActiveRecord
{

  /**
   * Linked tags ids
   * @var array
   */
  public $tags_id = [];

  /**
   * Linked files ids
   * @var array
   */
  public $files_id = [];

  /**
   * Linked collections ids
   * @var array
   */
  public $collections_id = [];

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'collections';
  }

  /**
   * @inheritdoc
   */
  public static function tagTableName()
  {
    return 'tag_to_collection';
  }

  /**
   * @inheritdoc
   */
  public static function fileTableName()
  {
    return 'file_to_collection';
  }

  /**
   * @inheritdoc
   */
  public static function collectionTableName()
  {
    return 'collection_to_collection';
  }

  /**
   * Retrieve all linked tags by collection id
   * @param int $id
   * @return array
   */
  public static function loadTags($id)
  {
    return TagModel::find()
        ->innerJoin(static::tagTableName(), 'tag_id=id')
        ->where(['collection_id' => (int) $id])
        ->orderBy(['title' => SORT_ASC])
        ->all();
  }

  /**
   * Retrieve all linked files by collection id
   * @param int $id
   * @return array
   */
  public static function loadFiles($id)
  {
    return FileModel::find()
        ->innerJoin(static::fileTableName(), 'file_id=id')
        ->where(['collection_id' => (int) $id])
        ->orderBy(['order' => SORT_ASC])
        ->all();
  }

  /**
   * Retrieve all linked collections by collection id
   * @param int $id
   * @return array
   */
  public static function loadCollections($id)
  {
    return static::find()
        ->innerJoin(static::collectionTableName(), 'collection_to_id=id')
        ->where(['collection_from_id' => (int) $id])
        ->orderBy(['order' => SORT_ASC])
        ->all();
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id'], 'integer'],
      [['title'], 'string', 'max' => Yii::$app->params['stringLength']],
      [['title'], 'required'],
      [['id', 'title'], 'unique'],
      [['tags_id', 'files_id', 'collections_id'], 'each', 'rule' => ['integer']],
      [['tags_id'], 'each', 'rule' => ['exist', 'targetAttribute' => 'id', 'targetClass' => TagModel::className()]],
      [['files_id'], 'each', 'rule' => ['exist', 'targetAttribute' => 'id', 'targetClass' => FileModel::className()]],
      [['collections_id'], 'each', 'rule' => ['exist', 'targetAttribute' => 'id', 'targetClass' => static::className()]],
    ];
  }

  /**
   * @inheritdoc
   */
  public function fields()
  {
    $fields = parent::fields();
    $fields[] = 'tags';
    $fields[] = 'files';

    return $fields;
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'title' => 'Title',
    ];
  }

  /**
   * Tags relation
   * @return yii\db\ActiveQuery
   */
  public function getTags()
  {
    return $this
        ->hasMany(TagModel::className(), ['id' => 'tag_id'])
        ->viaTable(static::tagTableName(), ['collection_id' => 'id']);
  }

  /**
   * Files relation
   * @return yii\db\ActiveQuery
   */
  public function getFiles()
  {
    return $this
        ->hasMany(FileModel::className(), ['id' => 'file_id'])
        ->viaTable(static::fileTableName(), ['collection_id' => 'id']);
  }

  /**
   * Collections from relation
   * @return yii\db\ActiveQuery
   */
  public function getCollectionsFrom()
  {
    return $this
        ->hasMany(static::className(), ['id' => 'collection_from_id'])
        ->viaTable(static::collectionTableName(), ['collection_to_id' => 'id']);
  }

  /**
   * Collections to relation
   * @return yii\db\ActiveQuery
   */
  public function getCollectionsTo()
  {
    return $this
        ->hasMany(static::className(), ['id' => 'collection_to_id'])
        ->viaTable(static::collectionTableName(), ['collection_from_id' => 'id']);
  }

  /**
   * @inheritdoc
   */
  public function afterSave($insert, $changedAttributes)
  {
    parent::afterSave($insert, $changedAttributes);

    $this->unlinkAll('tags', true);
    $this->tags_id = array_unique($this->tags_id);
    $tags = TagModel::findAll(['id' => $this->tags_id]);
    foreach ($tags as $tag) {
      $this->link('tags', $tag);
    }

    $this->unlinkAll('files', true);
    $this->files_id = array_unique($this->files_id);
    $files = ArrayHelper::index(FileModel::findAll(['id' => $this->files_id]), 'id');
    $order = 0;
    foreach ($this->files_id as $fileId) {
      if (array_key_exists($fileId, $files)) {
        $this->link('files', $files[$fileId], ['order' => ++$order]);
      }
    }

    $this->unlinkAll('collectionsTo', true);
    $this->collections_id = array_unique($this->collections_id);
    $collections = ArrayHelper::index(static::findAll(['id' => $this->collections_id]), 'id');
    $order = 0;
    foreach ($this->collections_id as $collectionId) {
      if (array_key_exists($collectionId, $collections)) {
        $this->link('collectionsTo', $collections[$collectionId], ['order' => ++$order]);
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function afterDelete()
  {
    parent::afterDelete();

    $this->unlinkAll('tags', true);
    $this->unlinkAll('files', true);
    $this->unlinkAll('collectionsTo', true);
    $this->unlinkAll('collectionsFrom', true);
  }

}
