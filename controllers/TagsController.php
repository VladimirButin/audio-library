<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\search\TagSearchModel;

class TagsController extends ActiveController
{

  public $modelClass = 'app\models\TagModel';
  public $serializer = [
    'class' => 'yii\rest\Serializer',
    'collectionEnvelope' => 'items',
  ];

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    // Application does not use any sort auth authentication
    unset($behaviors['authenticator']);
    unset($behaviors['rateLimiter']);

    return $behaviors;
  }

  public function actions()
  {
    $actions = parent::actions();
    $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

    return $actions;
  }

  public function prepareDataProvider()
  {
    return (new TagSearchModel())->search(Yii::$app->request->queryParams);
  }

  public function actionTypeahead()
  {
    return [
      'items' => (new TagSearchModel())->typeahead(Yii::$app->request->queryParams)
    ];
  }

}
