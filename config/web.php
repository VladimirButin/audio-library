<?php

$params = require(__DIR__ . '/params.php');

$config = [
  'id' => 'basic',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'components' => [
    'request' => [
      'cookieValidationKey' => 'enter cookie validation key',
      'parsers' => [
        'application/json' => 'yii\web\JsonParser',
      ]
    ],
    'cache' => [
      'class' => 'yii\caching\DummyCache',
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'db' => require(__DIR__ . '/db.php'),
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'suffix' => '/',
      'rules' => [
        ['class' => 'yii\rest\UrlRule', 'controller' => 'tags'],
      ],
    ],
    'assetManager' => [
      'forceCopy' => true,
    ],
  ],
  'params' => $params,
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'debug';
  $config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    'allowedIPs' => ['*']
  ];

  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
    'allowedIPs' => ['*']
  ];
}

return $config;
