<?php

use yii\db\Migration;
use app\models\TrackModel;

class m160510_160953_table_tracks extends Migration
{

  public function up()
  {
    $this->createTable(TrackModel::tableName(), [
      'id' => $this->primaryKey(),
      'path' => $this->string(Yii::$app->params['stringLength'])->notNull(),
      ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
  }

  public function down()
  {
    $this->dropTable(TrackModel::tableName());
  }

}
