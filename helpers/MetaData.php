<?php

namespace app\helpers;

use yii\helpers\ArrayHelper;
use getID3;
use app\models\StatsModel;

class MetaData
{

  /**
   * @var getID3
   */
  private static $getId3;

  /**
   * @var getID3
   */
  private static $getId3Hash;

  /**
   * @return getID3
   */
  private static function initGetId3()
  {
    if (static::$getId3 === null) {
      static::$getId3 = new getID3;
    }

    return static::$getId3;
  }

  /**
   * @return getID3
   */
  private static function initGetId3Hash()
  {
    if (static::$getId3Hash === null) {
      static::$getId3Hash = new getID3;
      static::$getId3Hash->setOption([
        'option_md5_data' => true,
        'option_sha1_data' => true,
      ]);
    }

    return static::$getId3Hash;
  }

  /**
   * Get all data by filename
   * @param string $filename
   * @return StatsModel
   */
  public static function getData($filename)
  {
    if (!file_exists($filename)) {
      return null;
    }

    $stats = static::initGetId3()->analyze($filename);
    if (!$stats || !ArrayHelper::getValue($stats, 'audio') || ArrayHelper::getValue($stats, 'video')) {
      return null;
    }

    $statsModel = new StatsModel();
    $statsModel->title = ArrayHelper::getValue($stats, 'tags.id3v1.title.0') ? : $statsModel->title;
    $statsModel->title = ArrayHelper::getValue($stats, 'tags.id3v2.title.0') ? : $statsModel->title;

    $statsModel->artist = ArrayHelper::getValue($stats, 'tags.id3v1.artist.0') ? : $statsModel->artist;
    $statsModel->artist = ArrayHelper::getValue($stats, 'tags.id3v2.artist.0') ? : $statsModel->artist;

    $statsModel->album = ArrayHelper::getValue($stats, 'tags.id3v1.album.0') ? : $statsModel->album;
    $statsModel->album = ArrayHelper::getValue($stats, 'tags.id3v2.album.0') ? : $statsModel->album;

    return $statsModel;
  }

  /**
   * Get hash value for file
   * @param type $filename
   * @return string
   */
  public static function getHash($filename)
  {
    if (!file_exists($filename)) {
      return '';
    }

    $stats = static::initGetId3Hash()->analyze($filename);
    if (!$stats || !ArrayHelper::getValue($stats, 'audio') || ArrayHelper::getValue($stats, 'video')) {
      return '';
    }

    return $stats['md5_data'] . '_' . $stats['sha1_data'];
  }

}
