<?php

namespace app\validators;

use yii\validators\Validator;

/**
 * Validation if file exists
 *
 * @author olre
 */
class FileExistsValidator extends Validator
{

  public function validateValue($value)
  {
    if (!file_exists($value)) {
      return ['File must exist by entered path', []];
    } elseif (!is_file($value)) {
      return ['Only files are allowed', []];
    }
    return [];
  }

}
