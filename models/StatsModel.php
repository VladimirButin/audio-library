<?php

namespace app\models;

use yii\base\Model;

/**
 * This is model for audio file stats
 */
class StatsModel extends Model
{

  public $title = "";
  public $artist = "";
  public $album = "";

}
