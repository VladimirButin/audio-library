(function () {
    // Tag form controller
    angular.module('alAppTags').controller('TagsFormController', TagsFormController);

    TagsFormController.$inject = ['TagsFormService'];

    function TagsFormController(TagsFormService) {
        var vm = this;

        vm.tag = TagsFormService.getCurrentObject();
        vm.submit = submit;
        vm.reset = reset;
        vm.isEditing = isEditing;

        function submit() {
            "use strict";
            TagsFormService.submit();
        }

        function isEditing() {
            "use strict";
            return TagsFormService.isEditing();
        }

        function reset() {
            "use strict";
            TagsFormService.reset();
        }
    }
})();