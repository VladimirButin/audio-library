(function () {
    angular.module('alAppTracks').directive('trackRow', trackRow);

    function trackRow() {
        return {
            restrict: 'EA',
            scope: {
                track: '<',
            },
            templateUrl: '/templates/track-row.html',
        };
    }
})();