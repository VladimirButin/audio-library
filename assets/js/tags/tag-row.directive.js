(function () {
    angular.module('alAppTags').directive('tagRow', tagRow);

    function tagRow() {
        return {
            restrict: 'EA',
            scope: {
                tag: '<',
            },
            templateUrl: '/templates/tag-row.html',
        };
    }
})();