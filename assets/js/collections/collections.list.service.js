(function () {
    // Collections list service
    angular.module('alAppCollections').service('CollectionsListService', getCollectionsListService);

    getCollectionsListService.$inject = ['BaseListService'];

    function getCollectionsListService(BaseListService) {
        "use strict";

        return new BaseListService;
    }
})();