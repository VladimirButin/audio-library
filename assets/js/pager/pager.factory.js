(function () {
    // Pagination
    angular.module('alAppPager').factory('PagerFactory', PagerFactory);

    class Pager {
        /**
         * Pager class
         *
         * @param handleRequest
         * @param handleData
         */
        constructor(handleRequest, handleData) {
            this.currentPage = 1;
            this.pageCount = 1;
            this.handleRequest = handleRequest;
            this.handleData = handleData;
        }

        getPage(pageNumber) {
            var self = this;

            self
                .handleRequest(pageNumber)
                .then(self.callback.bind(this, pageNumber));
        }

        callback(pageNumber, data) {
            this.currentPage = pageNumber;
            this.pageCount = data._meta.pageCount;
            this.handleData(data);
        }

        getFirstPage() {
            this.getPage(1);
        };

        getPrevPage() {
            this.getPage(Math.max(1, this.currentPage - 1));
        };

        getCurrentPage() {
            this.getPage(this.currentPage);
        };

        getNextPage() {
            this.getPage(Math.min(this.pageCount, this.currentPage + 1));
        };

        getLastPage() {
            this.getPage(this.pageCount);
        };
    }

    function PagerFactory() {
        return function (handleRequest, handleData) {
            return new Pager(handleRequest, handleData);
        };
    }
})();
