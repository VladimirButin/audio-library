(function () {
    // Tag form service
    angular.module('alAppTags').factory('TagsFormService', getTagsFormService);

    getTagsFormService.$inject = ['TagsCrudService', 'TagsListService', 'BaseFormService'];

    function getTagsFormService(TagsCrudService, TagsListService, BaseFormService) {
        "use strict";

        class TagsFormService extends BaseFormService {
        }

        return new TagsFormService(TagsCrudService, TagsListService);
    }
})();