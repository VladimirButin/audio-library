(function () {
    // Files resource
    angular.module('alAppFiles').factory('FilesResourceService', FilesResourceService);

    FilesResourceService.$inject = ['$resource'];

    function FilesResourceService($resource) {
        return $resource('/files/:action/', {}, {
            create: {
                method: 'POST',
                params: {
                    action: 'create',
                }
            },
            update: {
                method: 'PUT',
                url: '/files/:action/?id=:id',
                params: {
                    action: 'update',
                    id: '@id',
                }
            },
            list: {
                method: 'GET',
                params: {
                    action: 'index',
                    page: 1,
                },
            },
            typeahead: {
                method: 'GET',
                params: {
                    'action': 'typeahead',
                }
            },
            remove: {
                method: 'DELETE',
                params: {
                    action: 'delete',
                }
            },
            loadData: {
                method: 'GET',
                params: {
                    action: 'load-data'
                }
            },
        }, {
            stripTrailingSlashes: false,
        });
    }
})();