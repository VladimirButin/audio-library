(function () {
    angular.module('alAppTracks').factory('TracksCrudService', getTracksCrudService);

    getTracksCrudService.$inject = ['$q', 'TracksResourceService', 'MessagesService', 'BaseCrudService'];

    function getTracksCrudService($q, TracksResourceService, MessagesService, BaseCrudService) {
        "use strict";

        class TracksCrudService extends BaseCrudService {
            /**
             * @inheritDoc
             */
            init() {
                return {
                    id: "",
                    path: "",
                }
            }

            /**
             * @inheritDoc
             */
            prepareParams(object, withId) {
                var params = {
                    path: object.path
                };

                if (withId) {
                    params.id = object.id;
                }

                return params;
            }

            /**
             * @inheritDoc
             */
            messageText(object) {
                return 'Track "' + object.path + '"';
            }
        }

        return new TracksCrudService($q, TracksResourceService, MessagesService);
    }
})();
