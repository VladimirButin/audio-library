<?php

namespace app\components\playlist\commands;

use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Skip first n collections command
 *
 * @author olre
 */
class SkipCollectionsCommand implements CommandInterface
{

  /**
   * Skip value
   * @var int
   */
  public $skip = 0;

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    $generator->collections = array_slice($generator->collections, $this->skip);
  }

}
