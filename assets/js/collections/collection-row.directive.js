(function () {
    angular.module('alAppCollections').directive('collectionRow', collectionRow);

    function collectionRow() {
        return {
            restrict: 'EA',
            controller: 'CollectionRowController',
            controllerAs: 'vm',
            scope: {
                collection: '<',
                expand: '<',
            },
            templateUrl: '/templates/collection-row.html',
        };
    }
})();