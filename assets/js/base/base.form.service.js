(function () {
    class BaseFormService {
        /**
         * Base form service class
         *
         * @param CrudService
         * @param ListService
         */
        constructor(CrudService, ListService) {
            this._CrudService = CrudService;
            this._ListService = ListService;
            this._currentObject = this._CrudService.init();
            this._editing = false;
        }

        /**
         * Reset editing
         */
        reset() {
            this._editing = false;
            angular.copy(this._CrudService.init(), this._currentObject);
        }

        /**
         * Start editing object
         *
         * @param object
         */
        startUpdate(object) {
            this._editing = true;
            angular.copy(object, this._currentObject);
        }

        /**
         * Editing getter
         *
         * @returns {boolean}
         */
        isEditing() {
            return this._editing;
        }

        /**
         * Current object getter
         *
         * @returns {*}
         */
        getCurrentObject() {
            return this._currentObject;
        }

        /**
         * Submit form
         */
        submit() {
            (
                this.isEditing()
                    ? this._CrudService.update(this.getCurrentObject())
                    : this._CrudService.create(this.getCurrentObject())
            ).then(this._ListService.getRefresh());
        }
    }

    angular.module('alAppBase').value('BaseFormService', BaseFormService);
})();