(function () {
    // Collection form service
    angular.module('alAppCollections').factory('CollectionsFormService', getCollectionsFormService);

    getCollectionsFormService.$inject = ['CollectionsCrudService', 'CollectionsListService', 'TagsCrudService', 'FilesCrudService', 'BaseFormService'];

    function getCollectionsFormService(CollectionsCrudService, CollectionsListService, TagsCrudService, FilesCrudService, BaseFormService) {
        "use strict";

        function typeaheadCallback(data) {
            return data.items;
        }

        class CollectionsFormService extends BaseFormService {
            startUpdate(collection) {
                this._CrudService
                    .view(collection.id)
                    .then(this.loadData.bind(this));
            }

            loadData(data) {
                angular.copy(data, this.getCurrentObject());
                this._editing = true;
            }

            // Tags block

            typeaheadTags(title) {
                var params = {
                    title: title
                };

                return TagsCrudService.typeahead(params).then(typeaheadCallback);
            }

            addTag($item, $model, $label, $event) {
                if (this.getCurrentObject().tags.filter(tag => tag.id == $item.id).length == 0) {
                    this.getCurrentObject().tags.push($item);
                }
            }

            removeTag($index) {
                this.getCurrentObject().tags.splice($index, 1);
            }

            // Files block

            typeaheadFiles(path) {
                var params = {
                    path: path
                };

                return FilesCrudService.typeahead(params).then(typeaheadCallback);
            }

            addFile($item, $model, $label, $event) {
                if (this.getCurrentObject().files.filter(file => file.id == $item.id).length == 0) {
                    this.getCurrentObject().files.push($item);
                }
            }

            removeFile($index) {
                this.getCurrentObject().files.splice($index, 1);
            }

            // Collections block

            typeaheadCollections(title) {
                var params = {
                    title: title
                };

                return CollectionsCrudService.typeahead(params).then(typeaheadCallback);
            }

            addCollection($item, $model, $label, $event) {
                if (this.getCurrentObject().collections.filter(collection => collection.id == $item.id).length == 0) {
                    this.getCurrentObject().collections.push($item);
                }
            }

            removeCollection($index) {
                this.getCurrentObject().collections.splice($index, 1);
            }
        }

        return new CollectionsFormService(CollectionsCrudService, CollectionsListService);
    }
})();