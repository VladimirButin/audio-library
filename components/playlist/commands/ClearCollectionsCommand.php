<?php

namespace app\components\playlist\commands;

use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Clear collections command
 *
 * @author olre
 */
class ClearCollectionsCommand implements CommandInterface
{

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    $generator->collections = [];
  }

}
