<?php

namespace app\components\playlist\commands;

use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistGenerator;

/**
 * Shuffle collections command
 *
 * @author olre
 */
class ShuffleCollectionsCommand implements CommandInterface
{

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    shuffle($generator->collections);
  }

}
