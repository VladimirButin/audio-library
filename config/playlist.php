<?php

return [
    [
        'class' => 'app\components\playlist\commands\ClearPlaylistCommand',
    ],
    [
        'class' => 'app\components\playlist\commands\AddCollectionsCommand',
        'title' => 'artist:Isis',
    ],
    [
        'class' => 'app\components\playlist\commands\ExpandCollectionsCommand',
        'tags' => ['album'],
    ],
    [
        'class' => 'app\components\playlist\commands\RemoveTagsCommand',
        'tags' => ['unheard'],
    ],
    [
        'class' => 'app\components\playlist\commands\ExpandCollectionsCommand',
        'tags' => ['track'],
    ],
    [
        'class' => 'app\components\playlist\commands\AddTagsCommand',
        'tags' => ['unheard'],
    ],
//    [
//        'class' => 'app\components\playlist\commands\ShuffleCollectionsCommand',
//    ],
//    [
//        'class' => 'app\components\playlist\commands\LimitCollectionsCommand',
//        'limit' => 3,
//    ],
//    [
//        'class' => 'app\components\playlist\commands\ExpandCollectionsCommand',
//        'tags' => ['track'],
//    ],
//    [
//        'class' => 'app\components\playlist\commands\UniqueCollectionsCommand',
//    ],
//    [
//        'class' => 'app\components\playlist\commands\AppendPlaylistCommand',
//    ],
];
