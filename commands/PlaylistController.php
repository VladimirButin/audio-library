<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use app\components\cache\Cache;
use app\components\playlist\PlaylistGenerator;
use app\components\playlist\PlaylistWriter;

class PlaylistController extends Controller
{

  /**
   * Entry point
   */
  public function actionIndex()
  {
    ini_set('memory_limit', '-1');

    $generator = new PlaylistGenerator([
      'cache' => new Cache(),
      'commands' => array_map(function ($commandArray) {
          return Yii::createObject($commandArray);
        }, Yii::$app->params['playlistCommands']),
    ]);

    Console::output('Playlist generation started...');
    $generator->process();
    Console::output('...done');
  }

}
