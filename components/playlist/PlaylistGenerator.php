<?php

namespace app\components\playlist;

use yii\base\Object;
use app\components\cache\CacheInterface;

/**
 * Playlist generator
 *
 * @author olre
 */
class PlaylistGenerator extends Object implements PlaylistGeneratorInterface
{

  /**
   * Cache
   * @var CacheInterface
   */
  public $cache = null;

  /**
   * Currently loaded collections
   * @var array
   */
  public $collections = [];

  /**
   * Commands for playlist
   * @var Array
   */
  public $commands;

  /**
   * Process commands to generate playlist
   */
  public function process()
  {
    foreach ($this->commands as $command) {
      $command->execute($this, $this->cache);
    }
  }

}
