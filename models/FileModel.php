<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use app\helpers\MetaData;
use app\validators\FileExistsValidator;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $path
 * @property string $hash
 * @property string $title
 * @property string $artist
 * @property string $album
 */
class FileModel extends \yii\db\ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'files';
  }

  /**
   * Fetching row by path or hash
   * @param type $filename
   * @return FileModel
   */
  public static function fetch($filename)
  {
    $resultByPath = static::findByCondition(['path' => $filename])->one();
    if ($resultByPath) {
      return $resultByPath;
    }

    $resultByHash = static::findByCondition(['hash' => MetaData::getHash($filename)])->one();
    if ($resultByHash) {
      return $resultByHash;
    }

    return null;
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['path'], 'required'],
      [['path'], 'string', 'max' => Yii::$app->params['stringLength']],
      [['title', 'artist', 'album'], 'string', 'max' => Yii::$app->params['stringLength']],
      [['path'], FileExistsValidator::className()],
      [['path'], 'unique'],
      [['hash'], 'unique'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'path' => 'Path',
      'hash' => 'Hash',
      'title' => 'Title',
      'artist' => 'Artist',
      'album' => 'Album',
    ];
  }

  /**
   * Collections relation
   * @return yii\db\ActiveQuery
   */
  public function getCollections()
  {
    return $this
        ->hasMany(CollectionModel::className(), ['id' => 'collection_id'])
        ->viaTable(CollectionModel::fileTableName(), ['file_id' => 'id']);
  }

  /**
   * @inheritdoc
   */
  public function beforeValidate()
  {
    if (!parent::beforeValidate()) {
      return false;
    }

    $this->path = FileHelper::normalizePath($this->path);

    return true;
  }

  /**
   * @inheritdoc
   */
  public function beforeSave($insert)
  {
    if (!parent::beforeSave($insert)) {
      return false;
    }

    $this->hash = MetaData::getHash($this->path);

    return true;
  }

  /**
   * @inheritdoc
   */
  public function afterDelete()
  {
    parent::afterDelete();

    $this->unlinkAll('collections', true);
  }

}
