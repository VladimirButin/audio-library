<?php

namespace app\components\playlist\commands;

use app\components\cache\CacheInterface;
use app\components\playlist\PlaylistCollection;
use app\components\playlist\PlaylistGenerator;

/**
 * Expand collections command
 *
 * @author olre
 */
class ExpandCollectionsCommand implements CommandInterface
{

  /**
   * Array of title of tags
   * @var array
   */
  public $tags = [];

  /**
   * Exclude collections with tags instead of filtering
   * @var bool
   */
  public $exclude = false;

  /**
   * Resulting array
   * @var array
   */
  private $_result = [];

  /**
   * Array of tags ids
   * @var array
   */
  private $_tagsIds = [];

  /**
   * Execute command
   * @param PlaylistGenerator $generator
   * @param CacheInterface $cache
   */
  public function execute(PlaylistGenerator $generator, CacheInterface $cache)
  {
    $this->_tagsIds = [];
    foreach ($this->tags as $tagTitle) {
      $tag = $cache->getTag($tagTitle);
      if ($tag) {
        $this->_tagsIds[] = $tag->id;
      }
    }

    $this->_result = [];
    foreach ($generator->collections as $collection) {
      $this->expandRecursively($collection);
    }

    $generator->collections = $this->_result;
  }

  /**
   * Expand collection recursively and check tags
   * @param PlaylistCollection $collection
   */
  private function expandRecursively(PlaylistCollection $collection)
  {
    $hasTags = false;
    foreach ($collection->tags as $tag) {
      if (in_array($tag->id, $this->_tagsIds)) {
        $hasTags = true;
        break;
      }
    }

    if (($this->exclude && !$hasTags) || (!$this->exclude && $hasTags)) {
      $this->_result[] = $collection;
    } else {
      foreach ($collection->expand() as $relatedCollection) {
        $this->expandRecursively($relatedCollection);
      }
    }
  }

}
