(function () {
    // Collection form controller
    angular.module('alAppCollections').controller('CollectionsFormController', CollectionsFormController);

    CollectionsFormController.$inject = ['CollectionsFormService'];

    function CollectionsFormController(CollectionsFormService) {
        var vm = this;

        vm.collection = CollectionsFormService.getCurrentObject();
        vm.submit = submit;
        vm.reset = reset;
        vm.isEditing = isEditing;

        vm.typeaheadTags = typeaheadTags;
        vm.addTag = addTag;
        vm.removeTag = removeTag;
        vm.tagString = "";


        vm.typeaheadFiles = typeaheadFiles;
        vm.addFile = addFile;
        vm.removeFile = removeFile;
        vm.fileString = "";


        vm.typeaheadCollections = typeaheadCollections;
        vm.addCollection = addCollection;
        vm.removeCollection = removeCollection;
        vm.collectionString = "";


        function submit() {
            "use strict";
            CollectionsFormService.submit();
        }

        function isEditing() {
            "use strict";
            return CollectionsFormService.isEditing();
        }

        function reset() {
            "use strict";
            CollectionsFormService.reset();
        }

        function typeaheadTags(title) {
            "use strict";
            return CollectionsFormService.typeaheadTags(title);
        }

        function addTag($item, $model, $label, $event) {
            "use strict";
            CollectionsFormService.addTag($item, $model, $label, $event);
        }

        function removeTag($index) {
            "use strict";
            CollectionsFormService.removeTag($index);
        }


        function typeaheadFiles(title) {
            "use strict";
            return CollectionsFormService.typeaheadFiles(title);
        }

        function addFile($item, $model, $label, $event) {
            "use strict";
            CollectionsFormService.addFile($item, $model, $label, $event);
        }

        function removeFile($index) {
            "use strict";
            CollectionsFormService.removeFile($index);
        }


        function typeaheadCollections(title) {
            "use strict";
            return CollectionsFormService.typeaheadCollections(title);
        }

        function addCollection($item, $model, $label, $event) {
            "use strict";
            CollectionsFormService.addCollection($item, $model, $label, $event);
        }

        function removeCollection($index) {
            "use strict";
            CollectionsFormService.removeCollection($index);
        }

    }
})();