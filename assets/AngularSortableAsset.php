<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Angular sortable bundle
 */
class AngularSortableAsset extends AssetBundle
{

  public $sourcePath = '@bower/angular-ui-sortable';

  public $css = [];
  public $js = ['sortable.js'];
  public $jsOptions = ['position' => View::POS_HEAD];
  public $depends = [
    'app\assets\JqueryUIAsset',
  ];

}
