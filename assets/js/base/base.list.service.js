(function () {
    class BaseListService {
        /**
         * Base list service class
         */
        constructor() {
            this._refresh = function () {
            };
        }

        /**
         * Refresh setter
         * @param callback
         */
        setRefresh(callback) {
            if (callback instanceof Function) {
                this._refresh = callback;
            } else {
                throw new Error('Refresh callback must be function');
            }
        }

        /**
         * Refresh getter
         * @returns {BaseListService._refresh|*}
         */
        getRefresh() {
            return this._refresh;
        }
    }

    angular.module('alAppBase').value('BaseListService', BaseListService);
})();