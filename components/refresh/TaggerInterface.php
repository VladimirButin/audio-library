<?php

namespace app\components\refresh;

use app\models\CollectionModel;
use app\models\TagModel;
use app\models\FileModel;

/**
 * Tagger interface
 * @author olre
 */
interface TaggerInterface
{

  /**
   * Process file
   * @param FileModel $file
   * @param string $relativePath
   * @param string $fileName
   */
  public function process(FileModel $file, $relativePath, $fileName);
}
